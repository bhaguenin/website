const fs = require("fs");
const http = require("http");
const host = "localhost";
const port = 8080;
const server = http.createServer();

let comments = new Map();

function addComment(page,comment)
{
  console.log(page,comment);
  if (comments.has(page))
  {
    comments.get(page).push(comment);
  }
  else
  {
    comments.set(page,[comment]);
  }
  console.log(comments);
}

function getComment(page)
{
  if(comments.has(page))
  {
    return comments.get(page);
  }
  return [];
}


server.on("request", (req, res) => {
  if (req.url === "/stylesheet") {
    const stylesheet = fs.readFileSync("./style.css");
    res.end(stylesheet);
  } else if (req.url === "/logo") {
    const image = fs.readFileSync("./icon.jpg");
    res.end(image);
  } else if (req.url.startsWith("/images/small/")) {    //small images
    var urlBlocks = req.url.split("/");
    const image = fs.readFileSync("./images/image" + urlBlocks[urlBlocks.length - 1] + "_small.jpg");
    res.end(image);
  } else if (req.url.startsWith("/images/large/")) {    //large images
    var urlBlocks = req.url.split("/");
    const image = fs.readFileSync("./images/image" + urlBlocks[urlBlocks.length - 1] + ".jpg");
    res.end(image);
  } else if (req.url === "/images") {   //wall
    const images = fs.readdirSync("./images");
    let pageHTML = "<html><head><link rel=\"stylesheet\" type=\"text/css\" href=\"/stylesheet\"></head><body><a href=\"/index\"><button>Index</button></a><div>Mur d'Images</div><div id=\"wall\">"; //let pageHTML = "<!DOCTYPE html><html><body>";
    for (let i = 0; i < images.length; i++) {
      if (images[i].endsWith("_small.jpg")) {
        pageHTML += ("<a href=\"/image/" + Math.floor(i/2) + "\"><img src=\"/images/small/" + Math.floor(i/2) + "\"></a>");
      }
    }
    res.end(pageHTML);
  } else if (req.url.startsWith("/image/")) {   //individual pics
    const images = fs.readdirSync("./images");
	var urlBlocks = req.url.split("/");
	var img_index = urlBlocks[urlBlocks.length - 1].split("?")[0];
  console.log(img_index);
  let pageHTML = "<html><head><link rel=\"stylesheet\" type=\"text/css\" href=\"/stylesheet\"></head><body><a href=\"/images\"><button>Mur</button></a><div>"; //let pageHTML = "<!DOCTYPE html><html><body>";
  pageHTML+="<img src=\"/images/large/" + img_index + "\" width=500px>"
	pageHTML+="</div><div>cool picture</div><div>";
	pageHTML+="<a class=\"left\" href=\"/image/"+ Math.max(1,img_index-1) +"\"><button><img src=\"/images/small/"+Math.max(1,img_index-1) +"\"></button></a>";
	pageHTML+="<a class=\"right\" href=\"/image/"+ Math.min(images.length/2,+img_index+1) +"\"><button><img src=\"/images/small/"+Math.min(images.length/2,+img_index+1)+"\"></button></a></div>";
  pageHTML+="<div><form action=\"/comment/"+img_index+"\" method=\"get\"><fieldset><legend>Commentaire</legend><textarea name=\"comment\" rows=\"5\" cols=\"50\" required></textarea><br><input type=\"submit\" value=\"OK\"></fieldset></form></div>"

  let page_comments = getComment(img_index);
  console.log(page_comments);
  for (let i = 0;i<page_comments.length;++i)
  {
    pageHTML+="<div>"+page_comments[i]+"</div>";
  }
  res.end(pageHTML);
  } else if (req.url.startsWith("/comment/")) {   //comments
	var urlBlocks = req.url.split("/");
  var form = urlBlocks[urlBlocks.length - 1].split("?")[1];
	var img_index = urlBlocks[urlBlocks.length - 1].split("?")[0];
  var comment = form.substring(8);
  addComment(img_index,comment.replace("+"," "))
  res.statusCode = 302;
  res.setHeader('Location', '/image/'+img_index);
  res.end();
  } else {
    const index = fs.readFileSync("./index.htm", "utf-8");
    res.end(index);
  }
});
server.listen(port, host, () => {
  console.log(`Server running at http://${host}:${port}/`);
});
